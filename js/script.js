var prevScrollpos = window.pageYOffset;
var muno = document.querySelectorAll('header .container ul li a');

$(document).ready(function () {

  $(window).scroll(function () {
    var currentScrollPos = window.pageYOffset;
    if (prevScrollpos > currentScrollPos) {
      $("header").css('top',"0")
    } else {
      $("header").css('top',"-100%")
    }
    prevScrollpos = currentScrollPos;
  });

  $(muno).click(function () {
    for (i = 0; i < muno.length; i++) {
      $(muno[i]).css("border","none");
      $(this).css({ "border-bottom": "1px solid #ededed" });
    }
  });

  $(".muno-mobile").click(function(){
    $('ul.muno').css("right","0")
  });

  $("body").not("ul.muno").click(function(e){
    if(!$(e.target).is('.muno-mobile')){
      $('ul.muno').css("right","-100%")
    }
  })

  $(window).scroll(function () {
    var ps = $(this).scrollTop()
    if(ps > $('.skills').height() - $(window).height() + 300){
      for (var i = 0; i < $(".fill").length; i++) {
        var widthskill = $(".fill")[i].getAttribute('data-size')+"%";
        $(".fill")[i].style.width = widthskill
      }
    }
    
  })

});

//typed init
var options = {
  strings: ['من <span class="c-primary">احمد پور رستمی<br></span> طراح و برنامه نویس'],
  typeSpeed: 80,
  showCursor: false,
  cursorChar: '|',
  autoInsertCss: true,
};
var typed = new Typed('.type', options);